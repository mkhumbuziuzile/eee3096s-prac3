# Import libraries
import time
from typing import Tuple
import RPi.GPIO as GPIO
import random
import ES2EEPROMUtils
import os

# some global variables that need to change as we run the program
end_of_game = None  # set if the user wins or ends the game

# DEFINE THE PINS USED HERE
LED_value = [11, 13, 15]
LED_accuracy = 32
btn_submit = 16
btn_increase = 18
buzzer = 33
eeprom = ES2EEPROMUtils.ES2EEPROM()
guess = random.randint(0, pow(2, 3)-1) # Random starting number
value = 3
p = 0   # Light PWM
b = 0   # Buzzer PWM
add_score =[]   # Sore of thet game
tries = 0       # Number of guesses

# Print the game banner
def welcome():
    os.system('clear')
    print("  _   _                 _                  _____ _            __  __ _")
    print("| \ | |               | |                / ____| |          / _|/ _| |")
    print("|  \| |_   _ _ __ ___ | |__   ___ _ __  | (___ | |__  _   _| |_| |_| | ___ ")
    print("| . ` | | | | '_ ` _ \| '_ \ / _ \ '__|  \___ \| '_ \| | | |  _|  _| |/ _ \\")
    print("| |\  | |_| | | | | | | |_) |  __/ |     ____) | | | | |_| | | | | | |  __/")
    print("|_| \_|\__,_|_| |_| |_|_.__/ \___|_|    |_____/|_| |_|\__,_|_| |_| |_|\___|")
    print("")
    print("Guess the number and immortalise your name in the High Score Hall of Fame!")


# Print the game menu
def menu():
    global end_of_game
    global value
    global guess
    option = input("Select an option:   H - View High Scores     P - Play Game       Q - Quit\n")
    option = option.upper()
    if option == "H":
        os.system('clear')
        print("HIGH SCORES!!")
        s_count, ss = fetch_scores()
        display_scores(s_count, ss)
    elif option == "P":
        os.system('clear')
        print("Starting a new round!")
        print("Use the buttons on the Pi to make and submit your guess!")
        print("Press and hold the guess button to cancel your game")
        while (value == guess):
            value = generate_number()
        end_of_game = None
        while not end_of_game:
            pass
    elif option == "Q":
        print("Come back soon!")
        exit()
    else:
        print("Invalid option. Please select a valid one!")


def display_scores(count, raw_data):
    raw_data.sort(key=lambda x: x[1])
    print("There are {} scores. Here are the top 3!".format(count))
    for i in range(0,3):    #   Displaying scores
        print(i+1," - ",raw_data[i][0]," took ", raw_data[i][1],"guesses")
    pass


# Setup Pins
def setup():
    global p
    global b
    GPIO.setmode(GPIO.BOARD)
    for led in LED_value:
        GPIO.setup(led,GPIO.OUT)    #Setup LED as out out

    # Setup increase button
    GPIO.setup(btn_increase, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
    GPIO.add_event_detect(btn_increase, GPIO.RISING, callback=btn_increase_pressed,bouncetime=500)

    # Setup guess button
    GPIO.setup(btn_submit, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
    GPIO.add_event_detect(btn_submit, GPIO.RISING, callback=btn_guess_pressed,bouncetime=500)

    # Setup PWMs
    GPIO.setup(LED_accuracy, GPIO.OUT)
    p = GPIO.PWM(LED_accuracy, 100)
    GPIO.setup(buzzer, GPIO.OUT)
    b = GPIO.PWM(buzzer, 5)
    pass


# Load high scores
def fetch_scores():
    score_count = eeprom.read_byte(0)   # Read number of scores present
    scores = []                         # Array to stores Scores
    for i in range(1,score_count+1):
        data = eeprom.read_block(i,4)   
        score = data[3] 
        name = ""
        for v in range (0,3):
            name = name + chr(data[v])  # Conversion of name
        scores.append([name,score])
    return score_count, scores


# Save high scores
def save_scores():
    global add_score
    #   Reading/Fetching all scores
    score_count = eeprom.read_byte(0)
    scores = []
    for i in range(1,score_count+1):
        data = eeprom.read_block(i,4)
        score = data[3] 
        name = ""
        for v in range (0,3):
            name = name + chr(data[v])
        scores.append([name,score])
    
    # Adding new scores to array
    for i in range (0,len(add_score)):
        score_count += 1
        scores.append(add_score[i])

    #Clearing and writing all scores
    eeprom.clear(2048)
    eeprom.write_block(0, [score_count])
    scores.sort(key=lambda x: x[1])
    for i, score in enumerate(scores):
        data_to_write = []
        for letter in score[0]:
            data_to_write.append(ord(letter))
        data_to_write.append(score[1])
        eeprom.write_block(i+1, data_to_write)
    pass


# Generate guess number
def generate_number():
    return random.randint(0, pow(2, 3)-1)


# Increase button pressed
def btn_increase_pressed(channel):
    global guess
    guess +=1 # incrementing current guess(assumption) value
    GPIO.output(11,GPIO.LOW)
    GPIO.output(13,GPIO.LOW)
    GPIO.output(15,GPIO.LOW)

    if (guess>7): # if it greater than 7 (well =8) then start from zero
        guess = 0
    
    # Turning on LED as binary indicators of value
    VLED = guess
    if (VLED >= 4):
        VLED -= 4
        GPIO.output(15,GPIO.HIGH)

    if (VLED >= 2):
        VLED -= 2
        GPIO.output(13,GPIO.HIGH)
    
    if (VLED >= 1):
        VLED -= 1
        GPIO.output(11,GPIO.HIGH)
    pass

# Guess button
def btn_guess_pressed(channel):
    global end_of_game
    accuracy_leds()     # Toogle Accuracy LED
    trigger_buzzer()    # Toogle Buzzer
    
    
    ### If button is held use this loop to set holding reset time 
    hela = True
    sec = 0
    while (hela):
        sec = sec + 0.1
        time.sleep(0.2)
        hela = GPIO.input(btn_submit)   #Check if button is still held
        if (sec>0.7):       # After holding for at least 1.4 seconds break 
            hela = False

    global value
    global guess
    global tries
    global add_score
    tries =+ 1
    
    # If button was held long enough reset
    if (sec>0.6):
        reset()

    #   Check if guess and mystery value match then save score
    elif (value==guess):
        os.system('clear')
        print("CONGRATS YOU HAVE GUESSED THE CORRECT NUM!!!")
        name = input("Enter your name:")
        print("Please wait while saving score....")
        name = name[0:3]
        add_score.append([name,tries])
        save_scores()
        print("Going to menu ....")
        time.sleep(1)
        reset()
    pass


# LED Brightness
def accuracy_leds():
    global p
    bright = 0
    global value
    global guess
    if (value > guess):
        bright = (guess/value)*100
    elif (value < guess):
        bright = ((8-guess)/(8-value))*100
    elif(value==guess):
        bright = 100
    p.start(bright) # Set brightness of accuracy LED based on difference between guess and mystery value
    pass

# Sound Buzzer
def trigger_buzzer():
    global value
    global guess
    global b
    d = value - guess
    if (d<0):   # difference between guess and mystert value is negative turn it to be posetive
        d = d*-1

    if (d==1):
        b.ChangeFrequency(4)

    if (d==2):
        b.ChangeFrequency(2)

    if (d==3):
        b.ChangeFrequency(1)
    b.start(50) # Start with 50% duly cycle
    pass

#   Reseting game and taking back to main menu 
def reset():
    global end_of_game
    global tries
    global p
    global b
    end_of_game = True
    # Turn off LEDS and buzzer
    GPIO.output(11,GPIO.LOW)
    GPIO.output(13,GPIO.LOW)
    GPIO.output(15,GPIO.LOW)
    b.ChangeDutyCycle(0)
    p.ChangeDutyCycle(0)
    tries = 0   # reset number of time tried to guess number
    pass

# Main Function 
def main():
    try:
        # Call setup function
        setup()
        welcome()
        while True:
            menu()
            pass
    except Exception as e:
        print(e)
    finally:
        GPIO.cleanup()

if __name__ == "__main__":
    main()
